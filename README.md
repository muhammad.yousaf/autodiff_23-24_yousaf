# Autodiff_23-24_Yousaf



## Introduction

The `AutoDiff` project is designed to simplify the process of automatic differentiation for mathematical functions. It introduces a class, `AutoDiffFunction`, aimed at providing an intuitive interface for evaluating, performing arithmetic operations, concatenating, and differentiating mathematical functions.

## Project Overview

This project focuses on enabling users to work with mathematical functions easily without the need for external libraries for automatic differentiation or symbolic computation. Key objectives include:

- Developing a foundational class that supports a wide range of mathematical functions.
- Enabling straightforward arithmetic operations between functions.
- Supporting differentiation and evaluation of functions with ease.
- Offering functionalities like plotting, handling multi-dimensional inputs, and more.

## Features

### AutoDiffFunction - Class Definition
The `AutoDiffFunction` class, implemented within `autodiff.py`, lies at the core of the AutoDiff project. It is designed to facilitate automatic differentiation efficiently. Initialized with a function and its derivative, it supports:

- **Evaluation**: Computes the function's value at given points, accommodating scalar and vector-valued functions.
- **Differentiation**: Calculates derivatives or Jacobians, employing a numerical approach when an analytical derivative is unavailable.
- **Arithmetic Operations**: Overloads arithmetic operators to combine functions easily.
- **Advanced Functionalities**: Includes methods for creating piecewise functions, sums, and products of functions.

## Usage Examples

The project repository contains `examples.py`, showcasing practical applications of the `AutoDiffFunction` class. These examples range from basic usage to more complex scenarios involving multi-dimensional and vector-valued functions, demonstrating the class's capabilities in diverse mathematical computations.
