import numpy as np
import matplotlib.pyplot as plt
import functools

class AutoDiffFunction:
    def __init__(self, function, derivative=None, is_vector_valued=False):
        self.function = np.vectorize(function)
        self.derivative = derivative if derivative is not None else self._numerical_derivative
        self.is_vector_valued = is_vector_valued


    def evaluate(self, *args):
        if self.is_vector_valued:
            # For vector-valued functions, ensure we return a NumPy array
            return np.array(self.function(*args))
        else:
            return self.function(*args)

    def differentiate(self, *args, h=1e-5):
        if self.is_vector_valued:
            jacobian = []
            for i in range(len(self.function(*args))):
                partial_derivatives = []
                for j in range(len(args)):
                    def wrapped_func(*wrapped_args):
                        return self.function(*wrapped_args)[i]
                    partial_derivative = self._partial_derivative(wrapped_func, list(args), j, h)
                    partial_derivatives.append(partial_derivative)
                jacobian.append(partial_derivatives)
            return np.array(jacobian)
        else:
            if len(args) == 1:
                return np.vectorize(self._numerical_derivative)(args[0], h)
            else:
                return np.array([self._partial_derivative(self.function, list(args), i, h) for i in range(len(args))])


    def _numerical_derivative(self, x, h=1e-6):
        return (self.function(x + h) - self.function(x - h)) / (2 * h)


    def _partial_derivative(self, f, vars, idx, h):
        vars_plus_h = vars.copy()
        vars_minus_h = vars.copy()
        vars_plus_h[idx] += h
        vars_minus_h[idx] -= h
        return (f(*vars_plus_h) - f(*vars_minus_h)) / (2 * h)

    @functools.lru_cache(maxsize=128)
    def evaluate_cached(self, x):
        return self.evaluate(x)

    # Arithmetic operations
    def __add__(self, other):
        new_function = lambda x: self.evaluate(x) + other.evaluate(x)
        new_derivative = lambda x: self.differentiate(x) + other.differentiate(x)
        return AutoDiffFunction(new_function, new_derivative)

    def __mul__(self, other):
        new_function = lambda x: self.evaluate(x) * other.evaluate(x)
        new_derivative = lambda x: (self.differentiate(x) * other.evaluate(x) +
                                    self.evaluate(x) * other.differentiate(x))
        return AutoDiffFunction(new_function, new_derivative)

    def __truediv__(self, other):
        new_function = lambda x: self.evaluate(x) / other.evaluate(x)
        new_derivative = lambda x: (self.differentiate(x) * other.evaluate(x) -
                                    self.evaluate(x) * other.differentiate(x)) / other.evaluate(x)**2
        return AutoDiffFunction(new_function, new_derivative)

    # Concatenation and chain rule
    def compose(self, other):
        new_function = lambda x: self.evaluate(other.evaluate(x))
        new_derivative = lambda x: self.differentiate(other.evaluate(x)) * other.differentiate(x)
        return AutoDiffFunction(new_function, new_derivative)

    # Adding standard mathematical functions
    @staticmethod
    def polynomial(coeffs):
        """Creates a polynomial function with given coefficients."""
        def poly_function(x):
            return np.polyval(coeffs, x)

        def poly_derivative(x):
            # Create derivative coefficients
            d_coeffs = np.polyder(coeffs)
            return np.polyval(d_coeffs, x)

        return AutoDiffFunction(poly_function, poly_derivative)

    def exp(self):
        """Exponential function."""
        new_function = lambda x: np.exp(self.function(x))
        new_derivative = lambda x: np.exp(self.function(x)) * self.derivative(x)
        return AutoDiffFunction(new_function, new_derivative)

    def sin(self):
        """Sine function."""
        new_function = lambda x: np.sin(self.function(x))
        new_derivative = lambda x: np.cos(self.function(x)) * self.derivative(x)
        return AutoDiffFunction(new_function, new_derivative)

    def cos(self):
        """Cosine function."""
        new_function = lambda x: np.cos(self.function(x))
        new_derivative = lambda x: -np.sin(self.function(x)) * self.derivative(x)
        return AutoDiffFunction(new_function, new_derivative)


    def plot(self, x_range, num_points=100, plot_position=None, title=None):
        x_values = np.linspace(*x_range, num_points)
        y_values = self.evaluate(x_values)
        if plot_position:  # If subplot positioning is specified
            plt.subplot(*plot_position)
        plt.plot(x_values, y_values, label=title)
        plt.title(title if title else 'Function Plot')
        plt.xlabel('x')
        plt.ylabel('f(x)')
        plt.grid(True)
        if plot_position:  # Only show legend and tight layout for subplots
            plt.legend()
            plt.tight_layout()


    # Advanced operation: Piecewise function
    def piecewise(*conditions_and_funcs):
        """Creates a piecewise function from a list of (condition, function) tuples."""
        def piecewise_function(x):
            for condition, func in conditions_and_funcs:
                if condition(x):
                    return func.evaluate(x)
            return None  # Default case if no condition is met

        def piecewise_derivative(x):
            for condition, func in conditions_and_funcs:
                if condition(x):
                    return func.derivative(x)
            return None  # Default case if no condition is met

        return AutoDiffFunction(piecewise_function, piecewise_derivative)



    # Flexible argument method: Sum of functions
    @staticmethod
    def sum(*functions):
        """Calculates the sum of multiple AutoDiffFunction instances."""
        def sum_function(x):
            return np.sum([f.evaluate(x) for f in functions])

        def sum_derivative(x):
            return np.sum([f.differentiate(x) for f in functions])

        return AutoDiffFunction(sum_function, sum_derivative)

    ## Advanced Functionality: Sum and Product
    @staticmethod
    def product(*functions):
        """Calculates the product of multiple AutoDiffFunction instances."""
        def product_function(x):
            result = float(functions[0].evaluate(x))
            for f in functions[1:]:
                result *= f.evaluate(x)
            return result

        def product_derivative(x):
            sum_deriv = np.zeros_like(x)
            for i, fi in enumerate(functions):
                prod_deriv = fi.differentiate(x)
                for fj in functions:
                    if fi != fj:
                        prod_deriv *= fj.evaluate(x)
                sum_deriv += prod_deriv
            return sum_deriv

        return AutoDiffFunction(product_function, product_derivative)
