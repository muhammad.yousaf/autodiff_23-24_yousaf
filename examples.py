"""
This script showcases practical applications of the AutoDiffFunction class to demonstrate its capabilities, 
covering a range of scenarios from basic usage to more complex applications involving multi-dimensional and vector-valued functions.
"""
from autodiff import AutoDiffFunction
import numpy as np
import matplotlib.pyplot as plt
## Basic Function Evaluation and Differentiation
# Define a simple quadratic function f(x) = x^2
f = AutoDiffFunction(lambda x: x**2, lambda x: 2*x)

# Evaluate the function and its derivative at x = 2
print("f(2) =", f.evaluate(2))
print("f'(2) =", f.differentiate(2))

## Arithmetic Operations between Functions
# Define another function g(y) = sin(y)
g = AutoDiffFunction(np.sin, lambda y : np.cos(y))

# Perform arithmetic operations; here, f(y) = y**2
sum_func = f + g
prod_func = f * g
# Evaluate the new functions and their derivatives at x = pi/4
x_val = np.pi / 4
print("Sum function at x = pi/4:", sum_func.evaluate(x_val))
print("Product function at x = pi/4:", prod_func.evaluate(x_val))


## Plotting Functionality
# Prepare the figure for a 2x2 grid of plots
plt.figure(figsize=(10, 8))
# Plot each function in a subplot
f.plot(x_range=[-np.pi, np.pi], plot_position=(2, 2, 1), title='f(x) = x^2')
g.plot(x_range=[-np.pi, np.pi], plot_position=(2, 2, 2), title='g(x) = sin(x)')
sum_func.plot(x_range=[-np.pi, np.pi], plot_position=(2, 2, 3), title='Sum: f(x) + g(x)')
prod_func.plot(x_range=[-np.pi, np.pi], plot_position=(2, 2, 4), title='Product: f(x) * g(x)')
plt.show()


## Function Concatenation and Chain Rule
# Compose functions f and g: h(x) = f(g(x)) = (sin(x))^2
h = f.compose(g)
# Evaluate and differentiate the composite function at x = pi/4
print("h(pi/4) =", h.evaluate(x_val))
print("h'(pi/4) =", h.differentiate(x_val))


## Composite Functions with Chain Rule
# Example 1: Define the composite function C(x) = exp(sin(x)^2))
sin_func = AutoDiffFunction(np.sin, np.cos)
quad_func = AutoDiffFunction(lambda x: x**2, lambda x: 2*x)
exp_func = AutoDiffFunction(np.exp, np.exp)
composite = exp_func.compose(quad_func).compose(sin_func)

# Evaluate and differentiate the composite function at x = pi/4
x_val_trig = np.pi/4
print("C(pi/4) =", composite.evaluate(x_val_trig))
print("C'(pi/4) =", composite.differentiate(x_val_trig))

# Example 2: Function - C(x) = ln(1 + e^x)
add_func = AutoDiffFunction(lambda x: 1 + x)
ln_func = AutoDiffFunction(lambda x: np.log(x))

composite_func = ln_func.compose(add_func.compose(exp_func))

# Evaluate and differentiate the composite function at x = 0
x_val = 0
print("C(pi/4) =", composite_func.evaluate(x_val))
print("C'(pi/4) =", composite_func.differentiate(x_val))


## Univariate, Multivariate and Vector-Valued Functions
## Univariate Function
f = AutoDiffFunction(lambda x: x**2 + 2*x + 1)
print("f(3) =", f.evaluate(3))  # Evaluate at x=3
print("f'(3) =", f.differentiate(3))  # Differentiate at x=3


## Multivariate Function
g = AutoDiffFunction(lambda x, y: x**2 + y**2)
print("g(3, 4) =", g.evaluate(3, 4))  # Evaluate at (x=3, y=4)
print("∇g(3, 4) =", g.differentiate(3, 4))  # Differentiate at (x=3, y=4)


## Vector-valued Function
h = AutoDiffFunction(lambda x: [x**2, x**3], is_vector_valued=True)
print("h(2) =", h.evaluate(2))  # Evaluate at x=2
print("Jacobian of h at x=2 =\n", h.differentiate(2))  # Differentiate at x=2

# Define the multivariate, vector-valued function F(x, y) = [x^2 + y^2, 2*x*y]
F = AutoDiffFunction(lambda x, y: [x**2 + y**2, 2*x*y],
                     is_vector_valued=True)

# Evaluate the function at a point (x=1, y=2)
evaluation = F.evaluate(1, 2)
print(f'F(1, 2) = {evaluation}')

# Differentiate the function at the same point
differentiation = F.differentiate(1, 2)
print(f'Jacobian of F at (1, 2) = \n{differentiation}')


## Sum and Product of Functions
# Define a sum and product of f and g
f = AutoDiffFunction(lambda x: x**2, lambda x: 2*x)
g = AutoDiffFunction(np.sin, lambda y : np.cos(y))

# sum = x**2 + sin(x)
sum_of_funcs = AutoDiffFunction.sum(f, g)
# product = x**2 * sin(x)
prod_of_funcs = AutoDiffFunction.product(f, g)

# Evaluate and differentiate the sum and product at x = pi/4
x_val = np.pi/4
print("Sum of f and g at pi/4:", sum_of_funcs.evaluate(x_val))
print("Product of f and g at pi/4:", prod_of_funcs.evaluate(x_val))

## Piecewise Functionality
# Define a piecewise function that behaves differently on intervals
PW = AutoDiffFunction.piecewise(
    (lambda x: x < 0, AutoDiffFunction(lambda x: x**2)),
    (lambda x: x >= 0, AutoDiffFunction(np.sin))
)
PW.plot([-2, 2])
# Evaluate the piecewise function at x = -1 and x = 1
print("PW(-1) =", PW.evaluate(-1))
print("PW(1) =", PW.evaluate(1))
print("PW'(-1) =", PW.differentiate(-1))
print("PW'(1) =", PW.differentiate(1))


## Polynomial Functions
# Define a polynomial function P(x) = 3x^3 - 2x^2 + x - 5
coeffs = [3, -2, 1, -5]
p = AutoDiffFunction.polynomial(coeffs)

# Evaluate and differentiate the polynomial at x = 1
x_val_poly = 1
print("P(1) =", p.evaluate(x_val_poly))
print("P'(1) =", p.differentiate(x_val_poly))


## Trigonometric Identities
# Example : Define the trigonometric function T(x) = sin(x)cos(x)
sin_func = AutoDiffFunction(np.sin, np.cos)
cos_func = AutoDiffFunction(np.cos, lambda x: -np.sin(x))
T = sin_func * cos_func

# Evaluate and differentiate the function using the trigonometric identity at x = pi/4
x_val_trig = np.pi / 4
print("T(pi/4) =", T.evaluate(x_val_trig))
print("T'(pi/4) =", T.differentiate(x_val_trig))

# Example 2: Define the trigonometric function T(x) = (sin(x))^2 + cos(x)^2
square_fn = AutoDiffFunction(lambda x: x**2)
t1 = square_fn.compose(sin_func)
t2 = square_fn.compose(cos_func)

T2 = t1 + t2
print("T(p), where p ∈ (0, np.pi/4, np.pi/2, np.pi)")
print(T2.evaluate([0, np.pi/4, np.pi/2, np.pi]))
print("T'(p), where p ∈ (0, np.pi/4, np.pi/2, np.pi)")
print(T2.differentiate([0, np.pi/4, np.pi/2, np.pi]))